import Vue from 'vue'
import VueRouter from 'vue-router'
// components
import Login from '../components/Login.vue'
import MenuUser from '../components/MenuUser.vue'
import MenuApprover from '../components/MenuApprover.vue'
import MenuSystemAdmin from '../components/MenuSystemAdmin.vue'
import MenuUnit from '../components/MenuUnit.vue'
// views
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import MainUser from '../views/MainUser.vue'
import DetailMainUser from '../views/DetailMainUser.vue'
import ConfirmBooking from '../views/ConfirmBooking.vue'
import StatusUser from '../views/StatusUser.vue'
import MainApprover from '../views/MainApprover.vue'
import DetailApprover from '../views/DetailApprover.vue'
import Building from '../views/Building.vue'
import AddBuilding from '../views/AddBuilding.vue'
import EditBuilding from '../views/EditBuilding.vue'
import ManageUsers from '../views/ManageUsers.vue'
import Unit from '../views/Unit.vue'
import AddUnit from '../views/AddUnit.vue'
import DataUser from '../views/DataUser.vue'
import AddDataUser from '../views/AddDataUser.vue'
import EditDataUser from '../views/EditDataUser.vue'
import Room from '../views/Room.vue'
import AddRoom from '../views/AddRoom.vue'
import EditRoom from '../views/EditRoom.vue'
import TableApprover from '../views/TableApprover.vue'
import AddMangeApprove from '../views/AddMangeApprove.vue'
import Editmangeapprove from '../views/EditMangeApprove.vue'
import SubUnit from '../views/SubUnit.vue'
import AddunitSub from '../views/AddSubUnit.vue'
import EditunitSub from '../views/EditSubUnit.vue'
import MainUnit from '../views/MainUnit.vue'
import MainAdmin from '../views/MainAdmin.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login
  },
  {
    path: '/MenuUser',
    name: 'MenuUser',
    component: MenuUser
  },
  {
    path: '/MenuApprover',
    name: 'MenuApprover',
    component: MenuApprover
  },
  {
    path: '/MenuSystemAdmin',
    name: 'MenuSystemAdmin',
    component: MenuSystemAdmin
  },
  {
    path: '/MenuUnit',
    name: 'MenuUnit',
    component: MenuUnit
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/mainUser',
    name: 'MainUser',
    component: MainUser
  },
  {
    path: '/detailmainUser',
    name: 'DetailMainUser',
    component: DetailMainUser
  },
  {
    path: '/confirmBooking',
    name: 'ConfirmBooking',
    component: ConfirmBooking
  },
  {
    path: '/statusUser',
    name: 'StatusUser',
    component: StatusUser
  },
  {
    path: '/mainApprover',
    name: 'MainApprover',
    component: MainApprover
  },
  {
    path: '/detailApprover',
    name: 'DetailApprover',
    component: DetailApprover
  },
  {
    path: '/building',
    name: 'Building',
    component: Building
  },
  {
    path: '/addBuilding',
    name: 'AddBuilding',
    component: AddBuilding
  },
  {
    path: '/editBuilding',
    name: 'EditBuilding',
    component: EditBuilding
  },
  {
    path: '/manageUsers',
    name: 'ManageUsers',
    component: ManageUsers
  },
  {
    path: '/unit',
    name: 'Unit',
    component: Unit
  },
  {
    path: '/addUnit',
    name: 'AddUnit',
    component: AddUnit
  },
  {
    path: '/dataUser',
    name: 'DataUser',
    component: DataUser
  },
  {
    path: '/addDataUser',
    name: 'AddDataUser',
    component: AddDataUser
  },
  {
    path: '/editDataUser',
    name: 'EditDataUser',
    component: EditDataUser
  },
  {
    path: '/room',
    name: 'Room',
    component: Room
  },
  {
    path: '/addRoom',
    name: 'AddRoom',
    component: AddRoom
  },
  {
    path: '/editRoom',
    name: 'EditRoom',
    component: EditRoom
  },
  {
    path: '/tableApprover',
    name: 'TableApprover',
    component: TableApprover
  },
  {
    path: '/addmangeapprove',
    name: 'AddMangeApprove',
    component: AddMangeApprove
  },
  {
    path: '/editmangeapprove',
    name: 'Editmangeapprove',
    component: Editmangeapprove
  },
  {
    path: '/subunit',
    name: 'SubUnit',
    component: SubUnit
  },
  {
    path: '/addunitSub',
    name: 'AddunitSub',
    component: AddunitSub
  },
  {
    path: '/editunitSub',
    name: 'EditunitSub',
    component: EditunitSub
  },
  {
    path: '/mainUnit',
    name: 'MainUnit',
    component: MainUnit
  },
  {
    path: '/mainAdmin',
    name: 'MainAdmin',
    component: MainAdmin
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
