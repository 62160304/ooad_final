import api from './api'
export function getEvents (startTime, endTime) {
  return api.get('http://localhost:3000/bookings', { params: { startTime: startTime, endTime: endTime } })
}
